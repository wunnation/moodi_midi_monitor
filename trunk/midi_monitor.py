#!/usr/bin/env python

#include Standard Python Libs
import sys
import time
#include Gui
from PyQt4 import QtCore, QtGui
#include MIDI Backend
from midi.pypm_f import *

#include Custom MIDI Backends
from midi.pypm_class import MIDISystem

# Include All Widgets
from gui_midimonitor import MIDIMonitor_Window, MIDISend_Window

# Include gui images
from gui.find_images import GetImagesDirectory
# Include log file tools
from library.file_operations import WriteToLogFile, GenerateFileName
# Include versioning
from gui.sw_version import MOODI_MIDI_MONITOR_VERSION, VersioningWidgetClass

# Define Constants related to this main Window
APP_TITLE = "Moodi MIDI Monitor"
ERROR_MSG_MIDI_OUT_FAIL = "MIDI Output failed.\nCheck midi port selction.\n"
ERROR_MSG_MIDI_OUT_FAIL += "\n- if you continue to have problems, click the 'refresh MIDI Port list' arrow in the 'USB-MIDI Select' section."
PIXMAP_DIR = GetImagesDirectory()


# ========= MIDI Port Select Widget ==================================
# For Controlling the Connected MIDI Port (I/O Pair)
# Select ComboBox: Contains MIDI Port Pairs that the user can select
# Refresh Button: Used to Reset the MIDI System, and update the detected ports.
# This merely sets up the Layout, All functions are implemented in ProjectMainWindow
# ====================================================================
class MidiDeviceSelectWidget(QtGui.QFrame):
    def __init__(self, parent=None):
    #---- Initialize the GUI ---------
        super(MidiDeviceSelectWidget, self).__init__(parent)      # Calls the basic TextEdit Init Sequence
        self.setupRefreshButton(parent)
        self.setupMIDISelectCombobox(parent)
        self.createLayout(parent)

    def setupRefreshButton(self, parent):
        # Midi Port Select Refresh Button
        self.refresh_button = QtGui.QPushButton("Refresh", parent)
        self.refresh_button = QtGui.QToolButton(parent)
        self.refresh_button.setArrowType(3) # Points Left 
        self.refresh_button.setToolTip("Refresh MIDI Port List")

    def setupMIDISelectCombobox(self, parent):
        self.port_select = QtGui.QComboBox(parent) # single tab

    def createLayout(self, parent):
        self.layout = QtGui.QGridLayout()
        self.layout.addWidget(self.port_select,0,0)
        self.layout.addWidget(self.refresh_button,0,1)
        self.setLayout(self.layout)

# ========= Main Window ==================================
# Place All Widgets in Main Window
# MIDI System is also created and destroyed in this class
# =========================================================
class ProjectMainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        # Initialize the Main Window
        super(ProjectMainWindow, self).__init__(parent)      # Calls the basic TextEdit Init Sequence
        self.setWindowIcon(QtGui.QIcon(PIXMAP_DIR + 'moodi_midi_monitor_logo_square.png'))
        self.debug_output = False

        # Initialize GUI Space
        self.StartNewLogFile()
        self.AddTabsToMainWindow()

        # Initialize MIDI System Backend (Try to find a controller 3 times before starting up)
        self.InitMIDIVars()
        self.StartMIDISession()

        # Initialize MIDI Port Selection Boxes
        self.RefreshMidiInputSelect()    # Syncs the midi system and the Data Displayed in the MidiPortPairSelect Combo box

        # Initialize MIDI Monitor Poll Timer
        self.timer_midimon = QtCore.QTimer(self)                 # set up a timer
        self.timer_midimon.timeout.connect(self.checkMIDIPoll)   # when the timer goes off, run this function
        self.MIDI_MONITOR_LATENCY = 20                               # 1 (90% Processing), 2 (45% Processing), 3 (15% Processing), 5 (8%), 10 (negligeable) 
        #self.MIDI_MONITOR_LATENCY = 2                               # 1 (90% Processing), 2 (45% Processing), 3 (15% Processing), 5 (8%), 10 (negligeable) 
        self.MIDI_MONITOR_LATENCY_BOOTLOADING = 10
        self.timer_midimon.start(self.MIDI_MONITOR_LATENCY)                   # timer Length in ms

        # Initialize GUI Space (Part 2)
        self.SetupCentralWidget()  # Central Widget is the 'Tabbed' Interface
        #self.SetupStatusBar() # Status Bar for Displaying Messages to the User
        self.fixed_size = [650,500]
        rect = QtCore.QRect(25,25,25+self.fixed_size[0],25+self.fixed_size[1])
        self.setGeometry(rect)
        #self.setMinimumWidth(self.fixed_size[0])
        #self.setMinimumHeight(self.fixed_size[1])

        # Versioning: Set Software Version
        self.update_software_version()

    def dPrint(self, output_text): #Print Information to Debugging Log
        if self.debug_output:
            self.midimonitor_tab.text_out.append(output_text)
    

    def update_firmware_version(self, version_str):
        self.midimonitor_tab.versioning.firmware.setText(version_str)

    def update_software_version(self):
        version_string = self.midimonitor_tab.versioning.integer_to_version_string(MOODI_MIDI_MONITOR_VERSION)
        self.midimonitor_tab.versioning.software.setText(version_string)
        self.setWindowTitle(APP_TITLE + " v" + version_string)


    #============== GUI Elements =======================
    def AddTabsToMainWindow(self): # Must be called in __init__
        # Set Up Tab System
        # == Declare Widgets (to be placed in tabs) =====
        # ----- Midi Port Select Widget (dock-Top)----------------
        dockTop = QtGui.QDockWidget(self)
        self.midi_device_select = MidiDeviceSelectWidget()
        dockTop.setWidget(self.midi_device_select)
        dockTop.setFeatures(QtGui.QDockWidget.NoDockWidgetFeatures)
        dockTop.setFixedHeight(75) # Fix the Height of the MIDI Selection Widget so it is not expanded
        self.addDockWidget(QtCore.Qt.TopDockWidgetArea, dockTop)

        # External MIDI Controller
        self.midi_device_select.port_select.activated.connect(self.SelectMidiDevice) # Callback: Device Changed
        self.midi_device_select.refresh_button.clicked.connect(self.RestartMIDISession) 

        # ----- Midi Monitor Widget (dock-Right) --------------------
        self.midimonitor_tab = MIDIMonitor_Window() # non-dock
        self.midi_send_tab = MIDISend_Window() # non-dock
        #self.midimonitor_tab.buttons.debug.clicked.connect(self.ToggleDebuggingOutput)
        # - Midi filtering
        self.midi_channel_filter = range(16)
        self.midi_type_filter = [] # Empty List means no filtering
        self.midi_num_filter_min = 0
        self.midi_num_filter_max = 127
        self.midi_num_filter = range(self.midi_num_filter_min, self.midi_num_filter_max+1) # All Numbers 0-127 (min to max)
        # - Callbacks
        self.midimonitor_tab.buttons.midi_filter_channel.currentIndexChanged.connect(self.update_midi_channel_filter)  
        self.midimonitor_tab.buttons.midi_filter_type.currentIndexChanged.connect(self.update_midi_type_filter)  
        self.midimonitor_tab.buttons.midi_filter_num_min.valueChanged.connect(self.update_midi_num_filter_min)  
        self.midimonitor_tab.buttons.midi_filter_num_max.valueChanged.connect(self.update_midi_num_filter_max)  
        self.midi_send_tab.button.clicked.connect(self.send_midi_message_from_gui)
        self.midi_send_tab.text_box.returnPressed.connect(self.send_midi_message_from_gui_keypress)

    def SetupCentralWidget(self): # Must be called in __init__
        # Set up the Main Widget Layout (will be placed in main window)
        self.cw = QtGui.QWidget()
        self.cw_layout = QtGui.QVBoxLayout() # cw = central widget
        self.cw_layout.addWidget(self.midimonitor_tab)
        self.cw_layout.addWidget(self.midi_send_tab)
        self.cw.setLayout(self.cw_layout)
        # Set the Central Widget in the Main Window
        self.setCentralWidget(self.cw)

    def endProgram(self):  # Runs Just before application Exits
        # Stop all Qtimers
        self.timer_midimon.stop()
        time.sleep(.005)
        self.EndMIDISession()
    #============== GUI Elements ===================END=

    # ============== Files =======================
    def StartNewLogFile(self):
        self.mf_log_path = GenerateFileName("basev1_mf_log_")

    # ============== MIDI Filtering Callbacks ==============
    def update_midi_channel_filter(self, filter_channel_index):
        if filter_channel_index >= 16:
            print("Filter Channel All")
            self.midi_channel_filter = range(16) # Empty List means no filtering
        else:
            self.midi_channel_filter = [filter_channel_index]
            print("Filter Channel", filter_channel_index)
        pass
    def update_midi_type_filter(self, filter_type_index):
        if filter_type_index == 0:
            print("Filter Type All")
            self.midi_type_filter = [] # Empty List means no filtering
            self.midimonitor_tab.buttons.midi_filter_num_min.setDisabled(True)
            self.midimonitor_tab.buttons.midi_filter_num_max.setDisabled(True)
            self.midimonitor_tab.buttons.midi_filter_channel.setDisabled(True)
        elif filter_type_index == 1: # case Notes Only
            print("Filter Type Notes")
            self.midi_type_filter = [0x80,0x90] #range(0x80,0xA0) # Note on and Note off messages (all channels)
            self.midimonitor_tab.buttons.midi_filter_num_min.setDisabled(False)
            self.midimonitor_tab.buttons.midi_filter_num_max.setDisabled(False)
            self.midimonitor_tab.buttons.midi_filter_channel.setDisabled(False)
        elif filter_type_index == 2:
            print("Filter Type CCs")
            self.midi_type_filter = [0xB0] #range(0xB0,0xC0) # Note on and Note off messages (all channels)
            self.midimonitor_tab.buttons.midi_filter_num_min.setDisabled(False)
            self.midimonitor_tab.buttons.midi_filter_num_max.setDisabled(False)
            self.midimonitor_tab.buttons.midi_filter_channel.setDisabled(False)
            pass
        elif filter_type_index == 3:
            print("Filter Type Notes+CCs")
            self.midi_type_filter = [0x80, 0x90, 0xB0] #range(0xB0,0xC0) # Note on and Note off messages (all channels)
            self.midimonitor_tab.buttons.midi_filter_num_min.setDisabled(False)
            self.midimonitor_tab.buttons.midi_filter_num_max.setDisabled(False)
            self.midimonitor_tab.buttons.midi_filter_channel.setDisabled(False)
            pass

        else:
            print("Filter Type UNKNOWN", filter_type_index)
        pass
    def update_midi_num_filter_min(self, this_value):
        self.midi_num_filter_min = this_value
        self.midi_num_filter = range(self.midi_num_filter_min, self.midi_num_filter_max+1) # All Numbers 0-127 (min to max)
    def update_midi_num_filter_max(self, this_value):
        self.midi_num_filter_max = this_value
        self.midi_num_filter = range(self.midi_num_filter_min, self.midi_num_filter_max+1) # All Numbers 0-127 (min to max)
    # ============== MIDI Filtering Callbacks ==========END=
    # ============== MIDI Send Callbacks ===================
    def send_midi_message_from_gui_keypress(self):
        self.send_midi_message_from_gui(0)

    def send_midi_message_from_gui(self, this_value): # !review: this value is unused
        text_value = self.midi_send_tab.text_box.text()
        midi_message = self.midi_send_tab.text_box.parse_data_line(text_value)

        status = SendMIDIMessage(self.midi_out, midi_message)
        if status:
            text_out =  "Midi Send Failed: " + str(midi_message)
            self.midimonitor_tab.displayStatusMessage(text_out)
        else:
            #print "sent:", midi_message
            self.midimonitor_tab.displayMidiMessage(midi_message, time.time(), head_str='Sent')


    # ============= R&D: Debugging =================
    #def ToggleDebuggingOutput(self):
    #    self.debug_output ^= 1
    # ============= R&D: Debugging =============END=

    # ============= R&D: Staging =================
    # ============= R&D: Staging =============end=

    # ============== MIDI Session =========================
    # start_midi_selection
    def StartMIDISelection(self):
        pass

    def InitMIDIVars(self):
        self.midi_sys = False # MIDI Backend
        self.midi_out = False # Boolean Reads True when controller is connected
        self.wing_pairs = [] # Pair IDs of detected Guitar Wings
        self.midi_in = False # External MIDI Controller 
        self.midi_out = False # External MIDI Controller  (LED Feedback)

    def StartMIDISession(self):
        # Initialize MIDI Vars
        self.midi_sys = MIDISystem() # Initialize MIDI Systemx
        self.midimonitor_tab.text_out.append("midi session re-started. \n")
        self.midimonitor_tab.text_out.moveCursor(QtGui.QTextCursor.End) # Scroll to end of MIDI Monitor
        return True # 1 = Successful Connection        return True # successful connection

    def CloseMIDIPorts(self):
        print "closemidiports inert"

    def EndMIDISession(self):
        self.midi_in = False
        self.midi_out = False
        del self.midi_sys

    def RestartMIDISession(self):
        print "RestartMIDISession (somewhat inert)"
        self.EndMIDISession() # Shut down the MIDI Backend
        self.midimonitor_tab.text_out.append("midi session has ended. restarting...\n")
        self.midimonitor_tab.text_out.moveCursor(QtGui.QTextCursor.End) # Scroll to end of MIDI Monitor
        time.sleep(.002) # Sleep to ensure system is fully shutdown (not necessary, just precautionary)
        status = self.StartMIDISession()
        self.current_midi_port = 0 # Clear the currently selected MIDI Port
        # Store the connection information for the External MIDI Controller
        if self.midi_sys:
            current_input_num = self.midi_sys.last_midi_in_num
            current_input_str = self.midi_sys.last_midi_in_str
            #print "external MIDI Store:",current_input_num, '.', current_input_str
        else:
            current_input_str = ''
            current_input_num = 0xFF
        self.midi_device_select.port_select.clear() # Clear the midi port select List
        # Reconnect to the MIDI Through Port if it is still there
        if self.midi_sys and len(current_input_str):
            new_input_num = self.midi_sys.FindMidiInput_byString(current_input_str)
            if new_input_num < len(self.midi_sys.MidiInNumbers):
                #print "attempting to reconnect to ext"
                self.midi_in = self.midi_sys.ConnectToMidiInput(new_input_num)
                if self.midi_in:
                    out_string = "connected to: " + self.midi_sys.MidiPortInfo[self.midi_sys.MidiInNumbers[new_input_num]][1]
                else:
                    out_string = "could not reconnect to: " + self.midi_sys.MidiPortInfo[self.midi_sys.MidiInNumbers[new_input_num]][1]
            else:  
                    out_string = "did not find: " + current_input_str
            self.midimonitor_tab.text_out.append(out_string)
        # Refresh GUI with new MIDI Session Data
        self.RefreshMidiInputSelect()
        #self.UpdateStatusBar_PortStatus()
        return status

    # == MIDI Selection Combobox =================

    def SelectMidiDevice(self): # No LED Feedback
        #print "select ext"
        # Get the Newly Selected MIDI Port from the combobox
        midi_pair_num = self.midi_device_select.port_select.currentIndex()
        #midi_in_num = self.midi_device_select.port_select.currentIndex()
        # Close the old Midi Ports
        self.midi_in = False
        self.midi_out = False

        # Open the MIDI Input in the Midi System
        self.midi_in, self.midi_out = self.midi_sys.ConnectToMidiPair(midi_pair_num) # if 0xFF, does not connect to anything
        if self.midi_in and self.midi_out:
            if midi_pair_num < len(self.midi_sys.MidiPairs):
                out_string = "connected to: " + self.midi_sys.MidiPairs[midi_pair_num][2]
            else:
                out_string = "warning: connected to out of range device"
            self.midimonitor_tab.text_out.append(out_string)                

        # #self.midi_in = self.midi_sys.ConnectToMidiInput(midi_in_num) # if 0xFF, does not connect to anything
        # if not self.midi_in:
        #     self.midi_device_select.port_select.setCurrentIndex(len(self.midi_sys.MidiInNumbers))
        # else:
        #     if midi_in_num < len(self.midi_sys.MidiInNumbers):
        #         out_string = "connected to: " + self.midi_sys.MidiPortInfo[self.midi_sys.MidiInNumbers[midi_in_num]][1]
        #     else:
        #         out_string = "warning: connected to out of range device"
        #     self.midimonitor_tab.text_out.append(out_string)      

        # # Open the MIDI Output in the Midi System
        # self.midi_out = self.midi_sys.ConnectToMidiInput(midi_in_num) # if 0xFF, does not connect to anything
        # if not self.midi_out:
        #     self.midi_device_select.port_select.setCurrentIndex(len(self.midi_sys.MidiInNumbers))
        # else:
        #     if midi_in_num < len(self.midi_sys.MidiInNumbers):
        #         out_string = "connected to: " + self.midi_sys.MidiPortInfo[self.midi_sys.MidiInNumbers[midi_in_num]][1]
        #     else:
        #         out_string = "warning: connected to out of range device"
        #     self.midimonitor_tab.text_out.append(out_string)   



        # Inform the user of any pertinent results
        #self.UpdateStatusBar_PortStatus()
    # == MIDI Selection Combobox =============end=

    def checkMIDIPoll(self):
        if not self.midi_sys: 
            return

        #print time.time()
        updated_monitor = False 
        updated_graph = False  # Note updated_graph is not currently used
            
        # Read Messages from the External MIDI Controller
        message_queue, time_queue = self.midi_sys.ReadAllMessages(self.midi_in) # 0 = MIDI Controller Number
        # Respond to Messages from the External MIDI Controller
        for message_count in range(len(message_queue)):
            message = message_queue[message_count]
            cur_time = time_queue[message_count]  

            midi_type = message[0] & 0xF0
            midi_channel = message[0] & 0x0F
            matches_midi_type = False
            matches_midi_channel = False
            matches_midi_num = False

            if not self.midi_channel_filter or not self.midi_type_filter or midi_channel in self.midi_channel_filter:
                matches_midi_channel = True
            if not self.midi_type_filter or midi_type in self.midi_type_filter:
                matches_midi_type = True

            if not self.midi_num_filter:
                matches_midi_num = True
            elif len(message) > 1:
                if message[1] in self.midi_num_filter:
                    matches_midi_num = True

            if matches_midi_channel and matches_midi_type and matches_midi_num:
                self.midimonitor_tab.displayMidiMessage(message, time_queue[message_count], head_str='Received')
                updated_monitor = True

        if updated_monitor:
            self.midimonitor_tab.text_out.moveCursor(QtGui.QTextCursor.End) # Only update monitor once per Group of MIDI Messages

    # Returns: Whether or not you should Output to the MIDI Monitor
    def parseMIDIMessage(self, message, time):
        if len(message) < 3:        #!Revision: STREAM_TEST_MINIMUM_RESPONSE_LENGTH
            return False
        elif message[0] == 240: # Sysex Message
            sysex_display_return_val = True
            return sysex_display_return_val
        else:
            pass
            # Ordinary MIDI Messages
        return True # if it get's here, allow message to update MIDI Monitor
    # ============== MIDI Session =====================END=

    # ============== MIDI: GUI Update========================
    def RefreshMidiInputSelect(self, last_input_num= 0xFF, last_input_str = ''):    # Syncs the midi system and the Data Displayed in the MidiPortPairSelect Combo box
        if not self.midi_sys:
            return False
        # Copy pertinent properties from the midi system
        midi_in_ids = self.midi_sys.MidiInNumbers
        cur_conn_in = self.midi_sys.last_midi_in_num
        # Clear the midi port select List
        self.midi_device_select.port_select.clear()
        # Add an Entry for Each Port Pair
        for in_num in range(len(midi_in_ids)):
            entry_text = str(in_num) + '. ' + str(self.midi_sys.MidiPortInfo[self.midi_sys.MidiInNumbers[in_num]][1]) + '  '
            entry_text += str(self.midi_sys.MidiInNumbers[in_num])
            #print in_num, ". in select -> entry text:", entry_text
            self.midi_device_select.port_select.addItem(entry_text)
        # Add an Entry for 'Not Connected'
        self.midi_device_select.port_select.addItem("< Disconnected >")
        # Set the Combobox to show to the currently connected midi port pair
        if cur_conn_in < len(midi_in_ids) and self.midi_in: # Case: Valid MIDI Port is connected
            self.midi_device_select.port_select.setCurrentIndex(cur_conn_in) 
        else: # Case: 
            self.midi_device_select.port_select.setCurrentIndex(len(midi_in_ids)) 

    def RefreshMidiPortPairSelect(self):    # Syncs the midi system and the Data Displayed in the MidiPortPairSelect Combo box
        if not self.midi_sys:
            return False
        # Copy pertinent properties from the midi system
        midi_port_info = self.midi_sys.MidiPortInfo
        midipair_io_id = self.midi_sys.MidiPairs    #[[1, 0], [3, 2], [5, 4]]
        cur_conn_pair = self.midi_sys.ConnectedMidiPair
        # Clear the midi port select List
        self.midi_device_select.port_select.clear()
        # Add an Entry for Each Port Pair
        for pair_num in range(len(midipair_io_id)):
            entry_text = str(pair_num) + '. ' + str(self.midi_sys.MidiPortInfo[self.midi_sys.MidiPairs[pair_num][0]][1]) + '  '
            entry_text += str(self.midi_sys.MidiPairs[pair_num])
            self.midi_device_select.port_select.addItem(entry_text)
        # Add an Entry for 'Not Connected'
        self.midi_device_select.port_select.addItem("< Disconnected >")
        # Set the Combobox to show to the currently connected midi port pair
        if cur_conn_pair < len(midipair_io_id):
            self.midi_device_select.port_select.setCurrentIndex(cur_conn_pair) 
        else:
            self.midi_device_select.port_select.setCurrentIndex(len(midipair_io_id)) 
    # ============== MIDI: GUI Update====================END=

def main_program():
    app = QtGui.QApplication([APP_TITLE])
    mw = ProjectMainWindow()
    mw.show()
    app.aboutToQuit.connect(mw.endProgram)
    sys.exit(app.exec_())
    #app.deleteLater()	 

if __name__ == "__main__":
    main_program()

