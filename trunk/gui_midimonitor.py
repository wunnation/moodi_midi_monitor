import sys
import time
from PyQt4 import QtGui, QtCore
from midi.pypm_h import MIDI_MIN_MESSAGE_TYPE, MIDI_PITCHBEND
from gui.sw_version import MOODI_MIDI_MONITOR_VERSION, VersioningWidgetClass

INPUT_TYPE_BYTE_MULTIPLIER			=0x00
INPUT_TYPE_INCREMENTING_SERIES		=0x01
INPUT_TYPE_SINGLE_BYTE		=0xFF
MIDI_SEND_INVALID_INPUT = 99
MIDI_SEND_EMPTY_INPUT = 1023

#=============== MIDI Monitor Window (start) =============
# Main Class
#class MIDIMonitor_Window(QtGui.QWidget):
class MIDIMonitor_Window(QtGui.QGroupBox):
    def __init__(self, parent=None):
        super(MIDIMonitor_Window, self).__init__(parent)      # Calls the basic TextEdit Init Sequence
        self.setTitle("Received Messages/ Log")
        # Declare GUI Elements
        self.start_time = time.time()
        self.last_time = float(self.start_time)
        self.buttons = MIDIMonitor_Buttons()
        self.text_out = MIDIMonitor_TextBox()
        self.versioning = VersioningWidgetClass()

        # Declare Layout
        self.layout = QtGui.QVBoxLayout()
        #self.layout.addLayout(self.versioning.layout) # now just displayed in title bar
        self.layout.addWidget(self.text_out) # Place in Layout
        self.layout.addWidget(self.buttons) # Place in Layout
        self.setLayout(self.layout) # Set this Class' Layout

        # Declare MIDI Naming Standards
        self.MessageText =("Note Off","Note On","Key Prs","CC","Prog Ch","Chan Prs","Ptch Bnd","SysEx")
        self.MessageText_Realtime = ("Sysex","MTC","SPP","SS","Undef1","Undef2","Tune", "SysEx End", # indexed by "message_chan"
                                     "BeatClk","Undef3","Start","Continue","Stop","Undef4","ActSense","Reset")

        # Declare Button Actions
        self.buttons.clear_button.clicked.connect(self.text_out.clear)
       
        # Declare Configuration Constants
        #self.enable_time_display = False

    def displayMidiMessage(self, message, message_time, head_str='midi'): # Receives MIDI Message as List/ Translates to user readable onscreen display
        if not self.buttons.midi.isChecked(): # Disabled
            return False
        message_type = message[0] & 0xF0
        message_chan = message[0] & 0x0F
        output_text = head_str + ': '
        if message_type < 0x80:  # Invalid For Message to not begin with 0x80 flag
            output_text += "(Invalid): " + str(message) # Still Display it for debugging purposes
        elif message_type < 0xF0: # not: MIDI_SYSTEM_MESSAGE
            output_text += self.MessageText[(message_type>>4)-MIDI_MIN_MESSAGE_TYPE] + ' (c ' + str(message_chan+1) + ') '  
            if message_type == MIDI_PITCHBEND:
                output_text += '(v ' + str( (message[1] << 7) +  message[2] ) + ')'
            else:
                #output_text += str(message[1]) + ', ' + str(message[2])
                output_text += '(n ' + str(message[1]) + ') '
                output_text += '(v ' + str(message[2]) + ')'
        else: # 0xF0 (Realtime)
            #output_text += self.MessageText[(message_type>>4)-MIDI_MIN_MESSAGE_TYPE] + ': ' 
            output_text += str(message)

        if self.buttons.time.isChecked(): # print time (in milliseconds)
            if self.buttons.relative_time.isChecked():
                this_time = time.time()
                output_text += '	' + str((this_time-self.last_time)*1000)
                self.last_time = float(this_time)
            else:
                this_time = time.time()
                output_text += '	' + str((this_time-self.start_time)*1000)
                self.last_time = float(this_time)
        self.text_out.append(output_text) # Add the text to the midi_monitor

    def displayStatusMessage(self, message):
        self.text_out.append(message)
        self.text_out.moveCursor(QtGui.QTextCursor.End)

# Buttons in the MIDIMonitor_Window Class
#class MIDIMonitor_Buttons(QtGui.QGroupBox):
class MIDIMonitor_Buttons(QtGui.QFrame):
    def __init__(self, parent=None):
        # Allow Qt backend to Initialize the Parent Class
        super(MIDIMonitor_Buttons, self).__init__(parent)      # Calls the basic TextEdit Init Sequence

        # Declare Widgets
        self.clear_button = QtGui.QPushButton("clear")
        self.midi = QtGui.QCheckBox("midi")
        self.time = QtGui.QCheckBox("time")
        self.relative_time = QtGui.QCheckBox("relative")
        self.dev1 = QtGui.QPushButton("dev1")
        self.midi.setChecked(True) # midi is on by default
        # - Midi Filtering Widgets
        self.midi_filter_channel = MIDIMonitor_MidiChannel()
        self.midi_filter_type = MIDIMonitor_MidiType()
        self.midi_filter_num_min = MIDIMonitor_MidiNum(0)
        self.midi_filter_num_max = MIDIMonitor_MidiNum(127)
        self.midi_filter_num_min.setDisabled(True)
        self.midi_filter_num_max.setDisabled(True)
        self.midi_filter_channel.setDisabled(True)

        # Place Widgets in Layout
        self.layout = QtGui.QVBoxLayout()
        # - Declare Sub Layouts
        self.filter_hlayout = QtGui.QHBoxLayout()
        self.hlayout = QtGui.QHBoxLayout()
        # - Place Items in Layouts
        self.layout.addWidget(self.clear_button)
        # -- Filtering Sub layout
        self.filter_hlayout.addWidget(self.midi_filter_channel)
        self.filter_hlayout.addWidget(self.midi_filter_type)
        self.filter_hlayout.addWidget(self.midi_filter_num_min)
        self.filter_hlayout.addWidget(self.midi_filter_num_max)
        self.layout.addLayout(self.filter_hlayout)

        # -- Time Sub Layout
        self.hlayout.addWidget(self.time)
        self.hlayout.addWidget(self.relative_time)
        self.layout.addLayout(self.hlayout)
        self.setLayout(self.layout)

        # Set Default Settings
        self.time.setChecked(True)
        self.relative_time.setChecked(True)

class MIDIMonitor_MidiChannel(QtGui.QComboBox):
    def __init__(self, parent=None):
        super(MIDIMonitor_MidiChannel, self).__init__(parent)      # Calls the basic TextEdit Init Sequence
        for item_id in range(16):
            self.addItem("channel " + str(item_id+1))
        self.addItem("all channels")
        self.setCurrentIndex(16)


class MIDIMonitor_MidiType(QtGui.QComboBox):
    def __init__(self, parent=None):
        super(MIDIMonitor_MidiType, self).__init__(parent)      # Calls the basic TextEdit Init Sequence
        self.addItem("all messages")
        self.addItem("notes only")
        self.addItem("ccs only")
        self.addItem("notes+ccs only")
        self.setCurrentIndex(0)

class MIDIMonitor_MidiNum(QtGui.QSpinBox):
    def __init__(self, default_value, parent=None):
        super(MIDIMonitor_MidiNum, self).__init__(parent)      # Calls the basic TextEdit Init Sequence
        self.user_input_timer_value = QtGui.QSpinBox()
        self.setMinimum(0)
        self.setMaximum(127)
        self.setSingleStep(1)
        self.setValue(default_value)

# Textbox for use in the MIDIMonitor_Window Class
class MIDIMonitor_TextBox(QtGui.QTextEdit):
    def __init__(self, parent=None):
        super(MIDIMonitor_TextBox, self).__init__(parent)      # Calls the basic TextEdit Init Sequence
        #self.setPlainText("Midi Output Window:")
#=============== MIDI Monitor Window (end) ===============

#=============== MIDI Send Window (start) ================
# Main Class
class MIDISend_Window(QtGui.QGroupBox):
    def __init__(self, parent=None):
        super(MIDISend_Window, self).__init__(parent)      # Calls the basic TextEdit Init Sequence
        # Declare GUI Elements
        self.setTitle("Send MIDI Messages - sep: comma/space, */; ")
        self.text_box = MIDISend_TextBox()
        self.button = QtGui.QPushButton("<")

        # Declare Layout
        self.layout = QtGui.QHBoxLayout()
        self.layout.addWidget(self.text_box) # Place in Layout
        self.layout.addWidget(self.button) # Place in Layout
        self.setLayout(self.layout) # Set this Class' Layout

# Textbox for use in the MIDIMonitor_Window Class
class MIDISend_TextBox(QtGui.QLineEdit):
    def __init__(self, parent=None):
        super(MIDISend_TextBox, self).__init__(parent)      # Calls the basic TextEdit Init Sequence
        #self.setPlainText("Midi Output Window:")

    #------------------Input Processing-----------------------#
    def parse_data_line(self, text_value): # send_midi_message_from_gui
        message = []
        # - replace common separators with spaces, convert to ordinary string for splitting
        filtered_text = str(text_value.replace(',', ' ')) 
        split_text = filtered_text.split()
        # - convert each fragment into a midi message
        for this_text in split_text:
            message += self.parse_data_input_as_midi(this_text)
        #print "Converted Message:", message
        return message

    def convert_data_string_to_boundary_set(self, new_byte ):
        #--------------------------- Split Message By Operators -----------------------------------
        if ('*' in new_byte):		# Case: Multiple of the same Byte 
            input_string_type = INPUT_TYPE_BYTE_MULTIPLIER
            this_byte, other_byte = new_byte.split('*')
            if (';' in this_byte):  # Allow a repeating byte to alternate with a series
                this_byte, that_byte = this_byte.split(';')
            else:
                that_byte = ''
                
        elif ('/' in new_byte):     # Case: Series (Incrementing)
            input_string_type = INPUT_TYPE_INCREMENTING_SERIES
            this_byte, that_byte = new_byte.split('/')
            if (';' in that_byte): # Allow a repeating byte to alternate with a series
                # Example [0,99,1,99,2,99,3,99] is typed '0/3;99'
                that_byte, other_byte = that_byte.split(';')
            else:
                other_byte = ''		
        else:                       # Case: Single Byte
            input_string_type = INPUT_TYPE_SINGLE_BYTE
            this_byte,that_byte,other_byte = new_byte, '', ''
        #--------------------------- Split Message By Operators -------------------------_end_-----

        #--------------------------- Determine if is Acceptable Data -----------------------------------
        # Changes input_string_type to an error state, if data is invalid
        if not len(this_byte):
            this_byte_int = MIDI_SEND_EMPTY_INPUT
        else:
            try: this_byte_int = int(this_byte)
            except: input_string_type = MIDI_SEND_INVALID_INPUT #this_byte_int = MIDI_SEND_EMPTY_INPUT
        if not len(that_byte):
            that_byte_int = MIDI_SEND_EMPTY_INPUT
        else:
            try: that_byte_int = int(that_byte)
            except: input_string_type = MIDI_SEND_INVALID_INPUT #that_byte_int = MIDI_SEND_EMPTY_INPUT
        if not len(other_byte):
            other_byte_int = MIDI_SEND_EMPTY_INPUT
        else:
            try: other_byte_int = int(other_byte)
            except: input_string_type = MIDI_SEND_INVALID_INPUT #other_byte_int = MIDI_SEND_EMPTY_INPUT
        return this_byte_int,that_byte_int,other_byte_int,input_string_type
        #--------------------------- Determine if is Acceptable Data ------------------------end--------


    #--------------------------- Determine if is Acceptable Data ---------------------_end_---------
    def convert_boundary_set_to_list(self, this_byte, that_byte, other_byte, input_string_type):
        midi_message = []
        if input_string_type == MIDI_SEND_INVALID_INPUT:
            return []
        elif (input_string_type == INPUT_TYPE_BYTE_MULTIPLIER):                          # Multi-Message Operator Detected: Look for special Characters 
            i = 0					# InterpretSxMult()
            while i < other_byte:
                    midi_message.append(this_byte & 0x7F)
                    i+=1
                    if that_byte != MIDI_SEND_EMPTY_INPUT:
                        midi_message.append(that_byte & 0x7F)
        elif input_string_type == INPUT_TYPE_INCREMENTING_SERIES:
            this_byte = this_byte
            that_byte = that_byte              # !Revision: Allow Decrementing Too
            while this_byte < that_byte+1:
                midi_message.append(this_byte & 0x7F)
                this_byte+=1
                if other_byte != MIDI_SEND_EMPTY_INPUT:
                    midi_message.append(other_byte & 0x7F)
        elif input_string_type == INPUT_TYPE_SINGLE_BYTE:
            valid_input = 1
            midi_message.append(this_byte)
        elif input_string_type == MIDI_SEND_INVALID_INPUT:
            print "bad syntax: try again.", this_byte,that_byte,other_byte
        return midi_message

    def parse_data_input_as_midi(self, new_byte):		# revision: 2-sided (each side can have a * or / operator)
        this_byte,that_byte,other_byte,input_string_type = self.convert_data_string_to_boundary_set(new_byte)
        midi_message = self.convert_boundary_set_to_list(this_byte,that_byte,other_byte,input_string_type)
        return midi_message
    #------------------Input Processing-----------------END---#

#=============== MIDI Send Window (end) ==================


if __name__ == '__main__':
    app = QtGui.QApplication([])
    monitor_window = MIDIMonitor_Window()
    monitor_window.show()
    sys.exit(app.exec_())
