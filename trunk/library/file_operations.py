import time
# Returns a filename with the Current Time and Date Encoded
def GenerateFileName(file_path='logs/moodi_midi_monitor_'):
    #log_file_name = 'logs/Fader9Noise_'
    log_file_name = file_path
    log_file_time_string = time.ctime().lower().replace(':','').split()
    log_file_time_string = log_file_time_string[1] + log_file_time_string[2] + '_' + log_file_time_string[3] + '_' + log_file_time_string[4]
    log_file_name += log_file_time_string
    log_file_name += '.txt'
    return log_file_name

def WriteToLogFile(log_file_name, file_text):
    #---------- Write To File ------------
    try: 
        target = open(log_file_name, 'a')             # 'a' to append this line to the end of the file
    except:
        return 0

    target.write (file_text)
    target.close()                           # close file so that new data is saved
    #---------- Write To File --------end-
