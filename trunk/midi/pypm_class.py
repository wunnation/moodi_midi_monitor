# ==== pypm_class.py =======
# = pypm_class is a midi instance that can easily be created and deleted
from pypm_f import *

class MIDISystem():
    def __init__(self): # Routine that is run when the item is created
        #print "Midi Instance Initializing..."
        pypm.Initialize()  # Init Python Port MIDI Backend
        #print "Midi Instance Initialized..."
        self.MidiPortInfo, self.MidiInNumbers, self.MidiOutNumbers = GetMidiPortList()   # Get All MIDI Ports
        self.MidiPairs = GetMatchingMidiPorts_Exclusive(self.MidiPortInfo, self.MidiInNumbers, self.MidiOutNumbers)  # Get List of Matching MIDI Ports
        #print self.MidiPairs # [[1, 0], [3, 2], [5, 4]]
        self.ConnectedMidiPair = 0xFF
        self.ConnectionLatency = PYPM_DEF_CONNECT_LATENCY   # Define Connection Latency
        self.PerformanceLatency = PYPM_DEF_PERFORMANCE_LATENCY
        #self.ConnectedMidiControllers
        self.last_midi_in_num = 0xFF
        self.last_midi_in_str = ''
        self.last_midi_out_num = 0xFF
        self.last_midi_out_str = ''

    def DetectMidiControllers(self):
        self.MidiController, self.MidiController_ResponseTimes = GetMidiControllers(self.MidiPortInfo, self.MidiPairs, self.ConnectionLatency)

    def ConnectToMidiOutput(self, out_index): # For MIDI Through Function
        self.last_midi_out_num = 0xFF
        self.last_midi_out_str = ''
        #print "Outputs:", self.MidiOutNumbers
        if out_index < len(self.MidiOutNumbers):
            try:
                #print "out_index:", out_index
                midi_out = pypm.Output(self.MidiOutNumbers[out_index], self.PerformanceLatency) # open Midi Controller's Output
                self.last_midi_out_num = out_index
                self.last_midi_out_str = self.MidiPortInfo[self.MidiOutNumbers[out_index]][1]
            except:
                midi_out = False
            return midi_out
        else:
            #print "Invalid Output"
            return False

    def FindMidiInput_byString(self, port_string): # Returns Single Integer
        for in_num in range(len(self.MidiInNumbers)):
            if port_string.lower() in self.MidiPortInfo[self.MidiInNumbers[in_num]][1].lower(): # !review: case insensitive (currently)
                return in_num
        return 0xFF # 0xFF means not detected

    def ConnectToMidiInput(self, in_index): # For MIDI Through Function
        self.last_midi_in_num = 0xFF
        self.last_midi_in_str = ''
        #print "Inputs:", self.MidiInNumbers
        if in_index < len(self.MidiInNumbers):
            try:
                #print "in_index:", in_index
                midi_in = pypm.Input(self.MidiInNumbers[in_index], self.PerformanceLatency) # open Midi Controller's Output
                self.last_midi_in_num = in_index
                self.last_midi_in_str = self.MidiPortInfo[self.MidiInNumbers[in_index]][1]
            except:
                midi_in = False
            return midi_in
        else:
            #print "Invalid Input"
            return False

    def ConnectToMidiPair(self, pair_index):
        self.ConnectedMidiPair = 0xFF
        if pair_index < len(self.MidiPairs): # Valid Pair Index
            try:
                midi_in = pypm.Input(self.MidiPairs[pair_index][0]) #open Midi Controller's Input
                midi_out = pypm.Output(self.MidiPairs[pair_index][1], self.PerformanceLatency) # open Midi Controller's Output
            except:
                midi_in = False
                midi_out = False

            if midi_in and midi_out:
                #print "Pair is Open"
                self.ConnectedMidiPair = pair_index
                return midi_in, midi_out
            else:
                #print "Could Not Open One MIDI Port"
                return midi_in, midi_out
        else: # Pair Index out of Range
            return False, False

    def ConnectToMidiController(self, controller_index):
        self.ConnectedMidiPair = 0xFF # !revision: Auto-Detect/ Manual Select Combo -> connectedMidiPair to support this function
        if controller_index < len(self.MidiController):
            try:
                midi_in = pypm.Input(self.MidiController[controller_index][1]) #open Midi Controller's Input
                midi_out = pypm.Output(self.MidiController[controller_index][2], self.ConnectionLatency) # open Midi Controller's Output
                #!Review: Add Midi Port Pointers to self.MidiPortInfo table? or make self.PortPointers?
                return [midi_in, midi_out]        # Return Pointers to the MidiIn and MidiOut pypm objects
            except:     # !revision: ErrPmOpen?
                #print "Failed Connection -> In:", self.MidiController[controller_index][1], " Out:", self.MidiController[controller_index][2]
                return [False, False]	# Return 'False' if MIDI Port(s) Could not be opened
        else:
            return [False, False] # Return 'False' if MIDI Port(s) could not be opened

    def ReadAllMessages(self, midi_in):
        if midi_in:
            message_queue, time_queue = GetAllMidiMessages(midi_in)
            return message_queue, time_queue
        else:
            return [],[]


    def __del__(self): # Routine that is run when the item is deleted
        #print "Midi Instance Terminating...";
        pypm.Terminate()
        #print "Midi Instance Terminated...";


if __name__ == "__main__":
    for loop in range(10000):
        midi_system = MIDISystem()  # initialize midi class
        midi_system.DetectMidiControllers() # look for Controllers (currently inert)
        for controller in range (len(midi_system.MidiController)):   # Display Controllers detected (if any)
            controller_id = midi_system.MidiController[controller][0]
            if controller_id < len(ControllerNames):
                print loop, ": ", controller, ". Found: ", ControllerNames[controller_id]
        if not len(midi_system.MidiController):
            print loop, ": Found None"
        del midi_system
        time.sleep(.002)    

