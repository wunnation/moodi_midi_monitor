# pypm_f: This file is a minimized and optimized version of pypmExtensions_f.py
import pypm
import time
from pypm_h import *

#!review: create pypm error log for any errors we encounter
# force any 'except:' statement to write to it

#-------------Get MIDI Port List------------------
# Receives: 
#    Nothing 
#
# Returns:  List ( Driver Name, Port Name, Input?, Output?, Opened? )
#----------------------------------
def GetMidiPortList():                  #!Revision: Add Midi Numbers to the Packet
    Midi_Table = []
    Input_Numbers = []
    Output_Numbers = []
    for loop in range(pypm.CountDevices()):
        # Note: interf,name,inp,outp,opened = pypm.GetDeviceInfo(loop)
        try:
            current_device = pypm.GetDeviceInfo(loop)
        except:
            current_device = ["ERROR", "ERROR", 0, 0, 0]

        Midi_Table.append(current_device) # Add Device Info to the Table
        if current_device[2]:
            Input_Numbers.append(loop) # Add Midi Input Port Numbers to the Midi Input Tasble
        elif current_device[3]:
            Output_Numbers.append(loop) # Add Midi Output Port Numbers to the Midi Output Table
    return Midi_Table, Input_Numbers, Output_Numbers # Return the tables Midi_IO_Numbers
#-------------Get MIDI Port List--------------END-


#-------------Get Matching MIDI Port List Exclusive ------------------
# Receives: 
#    Nothing 
#
# Returns:  A List of Midi Ports with the same name ((Input Port 1, Output Port 1),( Input Port 2, Output Port 2), ...)
# - In the case of duplicates, this version, only allows each input to match to one output, 
# - and guesses that the lowest numbered input matches the lowest numbered output.
#----------------------------------
def GetMatchingMidiPorts_Exclusive(MidiPorts, MidiInputNumbers, MidiOutputNumbers):
    MidiPairs = []
    midi_match_found = [False]*len(MidiOutputNumbers)
    for port_in in range(len(MidiInputNumbers)):   # See if the names of any input ports match those of any output ports
        for port_out in range(len(MidiOutputNumbers)):
            if (MidiPorts[MidiInputNumbers[port_in]][1] == MidiPorts[MidiOutputNumbers[port_out]][1] and \
                    MidiPorts[MidiInputNumbers[port_in]][0] == MidiPorts[MidiOutputNumbers[port_out]][0]) and \
                    not midi_match_found[port_out]: # [0] = Driver Name [1] = Port Name
                MidiPairs.append([ MidiInputNumbers[port_in], MidiOutputNumbers[port_out], MidiPorts[ MidiInputNumbers[port_in]][1] ])
                midi_match_found[port_out] = True
                break # only allow one match per input
    return MidiPairs
#-------------Get Matching MIDI Port List Exclusive--------------END-


#-------------Get MIDI Controller List------------------
# Receives: 
#    Nothing 
#
# Returns:  All Recognized Controllers as a single list: 
#    [ [cn, InputNumber, OutputNumber],[cn, InputNumber, OutputNumber], ... ]
#----------------------------------
def GetMidiControllers(MidiPorts, MidiPairs, latency=PYPM_DEF_CONNECT_LATENCY):
    return [], [] # !revision: update to the standard method of identifying controllers
    # Initialize variables
    # Recognized_Controllers = []
    # response_times = []
    # cn = 0xFFFF
    # response_time = 0xFFFF

    # # Check each 'Pair' of Midi Ports
    # for midi_pair in range (len(MidiPairs)):
    #     cn, response_time = QueryMidiDevice(MidiPairs[midi_pair], latency=latency)
    #     if cn < len(ControllerNames):  # if Midi Pair Responded Properly to the query message
    #         Recognized_Controllers.append([cn, MidiPairs[midi_pair][0], MidiPairs[midi_pair][1], midi_pair])
    #         response_times.append(response_time)
    # return Recognized_Controllers, response_times  # Return Recognized Controller Number and how long it took to respond
#-------------Get MIDI Controller List--------------END-

#-------------Clear MIDI Buffer-------------------
def ClearMidiInBuffer(MidiIn):
    going = True
    while going:
    	try: 
            if MidiIn.Poll(): clearBuffer_rxmessage = GetOneMidiMessage(MidiIn)
            else: going = False
        except:
            going = False
#-------------Clear MIDI Buffer---------------END-



def QueryMidiDevice(InputOutputList, timeout=300, latency=25):
    return # !revision: upddate to use system identification sysex
    SysExVerifyEnabled = 1
    min = InputOutputList[0]
    mon = InputOutputList[1]
    cn = 0xFFFF # 99 -> Error Code: No Controller Found
    response_time = 0xFFFF
    try: midi_in = pypm.Input(min)  # !review: inputs do not openly support latency, but it can be assigned, but does this have an effect?
    except: return cn, response_time

    try: midi_out = pypm.Output(mon, latency)
    except: 
        del midi_in
        return cn, response_time
    #---------- Open Midi Ports -----------END

    #-----------Send SysEx Request And Get Response-----------------#
    output_message = GENERAL_MIDI_DEVICE_INQUIRY             #--- Prepare Output Message --------------------#
    expected_response = [0xF0, 0x00] #[0xF0, 0x00, id_1, id_2]          #--- Prepare Expected Response Message----------#
     
    sent_time = pypm.Time()                     # !Revision: Use time.time() because more reliable?

    ClearMidiInBuffer(midi_in)
    rx_response = SendMIDIMessage_ReturnResponse_ID(midi_in, midi_out, output_message, expected_response, timeout=300)
    #-----------Send SysEx Request And Get Response-------------END-#

    #---------------- Interpret Response -----------------------
    if len(rx_response) < MIDI_SYSEX_MIN_SIZE:  # !error code returned
        pass
    if rx_response == output_message:
        #print "Midi Thru Port: ", InputOutputList
        pass
    #elif rx_response[0:4] == CONTROLLER_HEADER:
    #    #print "\nController Found [", ControllerNames[cn], "]: ", rx_response
    #    response_time = pypm.Time()-sent_time
    #    #print "Response Received -> Delay: ",response_time, "ms"
    else:
        #print "Response Did Not Identify a Controller."
        pass
    #---------------- Interpret Response -----------------------
    del midi_in
    del midi_out
    return cn, response_time


# General Send Message
def SendOrdinaryMIDIMessage(MidiOut, output_message, silent=False):
    try:
        MidiOut.WriteShort(output_message[0], output_message[1], output_message[2])
        return 0
    except:
        if not silent:
            print "Send Ordinary MIDI Message: Failed to write", MidiOut
        return 1

def SendMIDIMessage(MidiOut, output_message, silent=True):
    if len(output_message) < 3: #Case: too short
        if not silent:
            print "invalid message length"
        return 0xFF
    elif not output_message[0] & 0x80: #Case: message does not start with correct flag for midi
        if not silent:
            print "invalid message header"
        return 0xFE
    elif output_message[0] != 0xF0: # case: non sysex
        if (SendOrdinaryMIDIMessage(MidiOut, output_message, silent=False)): return 0xFD
        if not silent:
            print "successfully forwarded (ordinary):", output_message
    else: # Case Sysex
        if (SendSysExMessage(MidiOut, output_message)): return ERR_PYPM_WRITE
        if not silent:
            print "successfully forwarded (sysex):", output_message
    return 0

#-------------Send Message/ Get Response-------------------
# Receives: MidiIn, ControllerNumber, Timeout(ms) (optional)
#
# Returns: Message Received if One is Received
# - Empty List if the operation Times Out
# Default TimeOut is 300ms
#----------------------------------
def SendMIDIMessage_ReturnResponse(MidiIn, MidiOut, output_message, expected_response=[], timeout=300, silent=False):
    if (SendSysExMessage(MidiOut, output_message)): return [[ERR_PYPM_WRITE]]

    response = GetSysExResponse(MidiIn, expected_response, timeout)
    return response

#def SendMIDIMessage_ReturnResponse_HCI(MidiIn, MidiOut, output_message, expected_response=[], timeout=300, silent=False):
#    if (SendSysExMessage(MidiOut, output_message)): return [[ERR_PYPM_WRITE]]

#    response = GetSysExResponse(MidiIn, expected_response, timeout)
#    return response
#-------------Send Message/ Get Response---------------END-

def SendMIDIMessage_ReturnResponse_ID(MidiIn, MidiOut, output_message, expected_response=[], timeout=300, silent=False):
    if (SendSysExMessage(MidiOut, output_message)): return [[ERR_PYPM_WRITE]]

    response = GetSysExResponse_ID(MidiIn, expected_response, timeout)
    return response


#-------------Send SysEx Message -------------------
# Receives: MidiIn, ControllerNumber, Timeout(ms) (optional)
#
# Returns: Message Received if One is Received
# - Empty List if the operation Times Out
#---------------------------------------------------
def SendSysExMessage(MidiOut, output_message):
    global max_len
    try:
        MidiOut.WriteSysEx(pypm.Time(), output_message)
    except:
        return True
    return False
#-------------Send SysEx Message---------------END-

#---------- Get SysEx Response ----------------
# Receives: MidiIn, ControllerNumber, Timeout(ms) (optional)
#
# Returns: Received Message if Received
# - []: Empty list on timeout
# - [[ERROR_CODE]]: List of Length 1 with Error Code upon Error
#
# Default TimeOut is 1000ms
#---------------------------------------------
def GetSysExResponse(MidiIn, expected_response, timeout=300):
    sent_time = pypm.Time()
    cntr = 1
    responded = False
    timed_out = False
    rx_message = []
    #print "expecting:",expected_response
    while not responded:
        #while not MidiIn.Poll() and not timed_out:
        # Wait for the next message, or for timeout
    	going = True
    	while going and not timed_out:
            try: 
                if MidiIn.Poll(): going = False #clearBuffer_rxmessage = GetOneMidiMessage(MidiIn)
                #else: going = True
            except:		# !revision: pmHostErr
                going = True	# !revision: Exit Routine? or Keep Going? or Return Error Message?
            time.sleep(.002)
            if (pypm.Time()-sent_time > timeout):  # !Revision: time.time for reliability?
                timed_out = True

        # if there is Midi In the Poll, get the message
        if not timed_out:
            index = 0
            rx_message = []
            rx_message = GetOneMidiMessage(MidiIn)
            if rx_message[0:len(expected_response)] == expected_response:
                responded = True
                response_time = pypm.Time()-sent_time
            else:
                if len(rx_message) >= 3:
                    #print "Garbage: ", rx_message 
                    pass
        else:
            rx_message = []
            responded = True
    return rx_message
#----------Get SysEx Response --END-

#---------- Get SysEx Response: with ID ----------------
# Receives: MidiIn, ControllerNumber, Timeout(ms) (optional)
#
# Returns: Received Message if Received
# - []: Empty list on timeout
# - [[ERROR_CODE]]: List of Length 1 with Error Code upon Error
#
# Default TimeOut is 1000ms
#---------------------------------------------
def GetSysExResponse_ID(MidiIn, expected_response, timeout=300, MESSAGE_ID = 8):
    sent_time = pypm.Time()
    cntr = 1
    responded = False
    timed_out = False
    rx_message = []
    while not responded:
        #while not MidiIn.Poll() and not timed_out:
        # Wait for the next message, or for timeout
    	going = True
    	while going and not timed_out:
            try: 
                if MidiIn.Poll(): going = False #clearBuffer_rxmessage = GetOneMidiMessage(MidiIn)
                #else: going = True
            except:		# !revision: pmHostErr
                going = True	# !revision: Exit Routine? or Keep Going? or Return Error Message?
            time.sleep(.002)
            if (pypm.Time()-sent_time > timeout):  # !Revision: time.time for reliability?
                timed_out = True

        # if there is Midi In the Poll, get the message
        if not timed_out:
            index = 0
            rx_message = []
            rx_message = GetOneMidiMessage(MidiIn)
            if rx_message[0:len(expected_response)] == expected_response and rx_message[len(expected_response)+1] == MESSAGE_ID:
                responded = True
                response_time = pypm.Time()-sent_time
                #print "Received Expected Message [", cntr, "]: ", rx_message
                #print "Delay: ",response_time, "ms"
        else:
            rx_message = []
            responded = True
            #print "timed out: " + str(pypm.Time()-sent_time) + "ms"
    return rx_message
#----------Get SysEx Response --END-

#--------- GetOneMidiMessage ------
# Requires: MidiInPoll()  (Message is ready to be read)
#
# Receives: MidiIn
#
# Returns:  Received Message  
# - 3 or 4 Bytes if is a normal MIDI Message
# - Entire Message if it is a SysEx Message
#----------------------------------
def GetOneMidiMessage(MidiIn, return_time = False):          #---Requires: MidiInPoll
    rx_message = []
    try:
        MidiData = MidiIn.Read(1)
    except:  #pmBufferOverflow or pmHostError: #Exception:                      #!Revision: Get the proper Error Code for Buffer Overflow
        finished = 1
        index = 0xFF
        return [[ERR_PYPM_BUFF_OVRFLW]]
    rx_message = MidiData[0][0]
    rx_time = MidiData[0][1]
    if len(rx_message):
        #message_type = MidiData[0][0][0]
        message_type = rx_message[0]
        if message_type == MIDI_SYSEX:              # read only 4 bytes at a time... Our SysEx are a minimum of 7 Bytes
            rx_message = FinishSysEx(MidiIn, rx_message)
    if not return_time:
        return rx_message
    else:# return_time:
        return rx_message, rx_time
#---------- Get One Full Message -----END----------

#-------------GetAllMidiMessages-------------------
def GetAllMidiMessages(MidiIn):
    going = True
    message_queue = []
    time_queue = []
    while going:
    	try: 
            if MidiIn.Poll(): 
                message, time = GetOneMidiMessage(MidiIn, return_time = True)
                message_queue.append(message)
                time_queue.append(time)
            else: going = False
        except:
            going = False
    return message_queue, time_queue
#-------------GetAllMidiMessages---------------END-


#-------- Finish SysEx ------------------
# Requires: Rest of SysEx to be InPoll
#
# Receives: 
#    MidiIn: Midi Input Object
#    rx_message: First 4-Bytes of a SysEx Message (as read by MidiIn.Read(1))
#
# Returns:  Received Message
# - Entire SysEx Message
#----------------------------------
def FinishSysEx(MidiIn, rx_message):            # We have first four Bytes, now we must read for the next four Bytes
    # initialize data packet
    finished = 0
    # get all packets until the sysex message is complete
    while not finished:            # We have first four Bytes, now we must read Four Byte Blocks until the message is finished (receive 247).
        index = 0
        # get one packet
        try: 
            if MidiIn.Poll():
                data = MidiIn.Read(1)
            else:
                # no midi data, return what we've got so far.
                return rx_message
        except: # pmBufferOverflow, pmHostErr:     #!Review: Is this the way to deal with a Buffer Error?  -9969
            # Error in MIDI Read: Return only an error code
            finished = 1
            index = 0xFF
            return [[ERR_PYPM_BUFF_OVRFLW]]

        # Analyze Packet (look for sysex end)
        while index < 4:
            rx_message.append(data[0][0][index])
            if data[0][0][index] == MIDI_SYSEX_END:
                # This is the end of the sysex message.
                index = 0xFF
                finished = 0xFF
            else:
                index+=1
    return rx_message
#-------- Finish SysEx -------------END--
