# ==== pypm_class.py =======
# = pypm_class is a midi instance that can easily be created and deleted
import rtmidi
#from rtmidi_f import *

def GetMidiPortNames():
    print "GetMidiPortList"
    midi_out = rtmidi.MidiOut()
    midi_in = rtmidi.MidiIn()
    out_port_names = midi_out.get_ports()
    in_port_names = midi_in.get_ports()
    #self.midi_pairs = []
    # all_ports = in_ports + out_ports
    return in_port_names, out_port_names

def GetMidiPortPairs(midi_in_port_names, midi_out_port_names):
    midi_pairs = []
    midi_match_found = [False]*len(midi_out_port_names)
    for port_in in range(len(midi_in_port_names)):   # See if the names of any input ports match those of any output ports
        for port_out in range(len(midi_out_port_names)):
            if (midi_in_port_names[port_in] == midi_out_port_names[port_out]) and not midi_match_found[port_out]: # [0] = Driver Name [1] = Port Name
                midi_pairs.append([port_in, port_out, midi_out_port_names[port_out]])
                midi_match_found[port_out] = True
                break # only allow one match per input
    print "MIDI Pairs", midi_pairs
    return midi_pairs

class MIDISystem():
    def __init__(self): # Routine that is run when the item is created
        self.MAX_DEVICE_CONNECTIONS = 2
        #print "Midi Instance Initializing..."
        #pypm.Initialize()  # Init Python Port MIDI Backend
        #print "Midi Instance Initialized..."
        self.midi_in_port_names, self.midi_out_port_names = GetMidiPortNames()
        #self.MidiPortInfo, self.MidiInNumbers, self.MidiOutNumbers = GetMidiPortList()   # Get All MIDI Ports
        self.MidiPairs = GetMidiPortPairs(self.midi_in_port_names, self.midi_out_port_names)  # Get List of Matching MIDI Ports
        #print self.MidiPairs # [[1, 0], [3, 2], [5, 4]]
        self.ConnectedMidiPair = 0xFF
        # self.ConnectionLatency = PYPM_DEF_CONNECT_LATENCY   # Define Connection Latency
        # self.PerformanceLatency = PYPM_DEF_PERFORMANCE_LATENCY
        #self.ConnectedMidiControllers
        self.last_midi_in_num = 0xFF
        self.last_midi_in_str = ''
        self.last_midi_out_num = 0xFF
        self.last_midi_out_str = ''

        self.midi_inputs = [rtmidi.MidiIn()]*self.MAX_DEVICE_CONNECTIONS
        self.midi_outputs = [rtmidi.MidiOut()]*self.MAX_DEVICE_CONNECTIONS

    def open_midi_out(self, out_connection_id, out_index):
        status = self.midi_outputs[out_connection_id].open_port(out_index)
        print "open midi out:", status

    def open_midi_in(self, in_connection_id, in_index):
        status = self.midi_inputs[in_connection_id].open_port(in_index)
        print "open midi in:", status

    # def GetMidiPairName(self, input_id):

    def find_midi_input_by_string(self, port_string): # Returns Single Integer
        for in_num in range(len(self.midi_in_port_names)):
            if port_string.lower() in self.midi_in_port_names[in_num]: # !review: case insensitive (currently)
                return in_num
        return 0xFF # 0xFF means not detected

    def assign_midi_in_callback(self, connection_id, callback_function):
        self.midi_inputs[connection_id].set_callback(callback_function)


    def __del__(self): # Routine that is run when the item is deleted
        pass
        #print "Midi Instance Terminating...";
        #pypm.Terminate()
        #print "Midi Instance Terminated...";


if __name__ == "__main__":
    def test_callback(event, data=None):
        print "test_callback", event, data
    
    midi_system = MIDISystem()
    midi_system.open_midi_in(0,0)
    midi_system.open_midi_out(0,0)
    midi_system.assign_midi_in_callback(0,test_callback)
  
while (1):
    pass