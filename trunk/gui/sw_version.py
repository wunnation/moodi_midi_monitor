from PyQt4 import QtGui, QtCore

MOODI_MIDI_MONITOR_VERSION = 107

class VersioningWidgetClass(QtGui.QWidget):
    def __init__(self, parent=None):
        self.firmware = QtGui.QLabel('0.00')
        self.software = QtGui.QLabel('0.00')
        self.firmware_LABEL = QtGui.QLabel('firmware')
        self.software_LABEL = QtGui.QLabel('version')
        self.align_labels()
        self.layout = QtGui.QGridLayout()
        self.layout.addWidget(self.software_LABEL,0,0)
        self.layout.addWidget(self.software,0,1)

    def align_labels(self):
        self.firmware.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)
        self.firmware_LABEL.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignBottom)
        self.software.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)
        self.software_LABEL.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignBottom)

    def integer_to_version_string(self, firmware_version):
        firmware_string = str(firmware_version)
        if len(firmware_string) < 3:
            firmware_string = '0.' + firmware_string # pad with '0.'
        else:
            firmware_string = firmware_string[0:len(firmware_string)-2] + '.' + firmware_string[len(firmware_string)-2:len(firmware_string)]
        return firmware_string

