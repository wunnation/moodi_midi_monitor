import os
import sys

# ========== GetImagesDirectories ============
def GetMacWorkingDirectory():
    CURRENT_DIR_PATH = os.path.dirname(sys.executable)
 
    MAC_APP_STRING = '.app/Contents/MacOS'
    PYTHON_LIBRARY_STRING = '/library'
    index  = CURRENT_DIR_PATH.rfind(MAC_APP_STRING)
 
    if index > 0:   # If Project Has been built into an app (If Not: index is -1)
        MPHIDFLASH_DIR_STRING_LENGTH = CURRENT_DIR_PATH.rfind(MAC_APP_STRING) + len(MAC_APP_STRING)
        MPHIDFLASH_DIR_PATH = CURRENT_DIR_PATH[:MPHIDFLASH_DIR_STRING_LENGTH]
        return MPHIDFLASH_DIR_PATH + '/'
    else:           # If Project has Not been built into an app (run via installed$
        return ''
# ========== GetImagesDirectories ========end=

def GetMacApplicationDirectory():
    CURRENT_DIR_PATH = os.path.dirname(os.path.abspath(__file__))
    MAC_APP_STRING = '.app/Contents/MacOS'
    PYTHON_LIBRARY_STRING = '/library'
    index  = CURRENT_DIR_PATH.rfind(MAC_APP_STRING)

    if index > 0:   # If Project Has been built into an app (If Not: index is -1)
        APPLICATION_DIR_STRING_LENGTH = CURRENT_DIR_PATH.rfind(MAC_APP_STRING) + len(MAC_APP_STRING)
        APPLICATION_DIR_PATH = CURRENT_DIR_PATH[:APPLICATION_DIR_STRING_LENGTH]
        #print 'Built App: Calculated Application Dir Path -> ' + APPLICATION_DIR_PATH
        return APPLICATION_DIR_PATH
    else:           # If Project has Not been built into an app (run via installed Python)
        return ''

def GetImagesDirectory():
    # ========== Project Configuration Constants =================
    if 'darwin' in sys.platform.lower():
        PIXMAP_DIR_BASE = GetMacWorkingDirectory()
        #PIXMAP_DIR = PIXMAP_DIR_BASE + "gui/images/"
        PIXMAP_DIR = PIXMAP_DIR_BASE + "gui/image_lib/"
    else:
        #PIXMAP_DIR = "gui/images/"
        PIXMAP_DIR = "gui/image_lib/"
    return PIXMAP_DIR
