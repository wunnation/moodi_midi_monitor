#!/usr/bin/env python

#include Standard Python Libs
import sys
import time
#include Gui
from PyQt4 import QtCore, QtGui


class GUI_ExtMc_ActionSel():
    def __init__(self):
        #self.layout.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter) # Mac OS X Suggested Patch
        self.ACTION_TEXTS =("connect","reset all conn")
        self.label = QtGui.QLabel("action")
        self.label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.label.setFrameStyle(6) # Thin Box around it
        self.combobox = QtGui.QComboBox()#["Note On", "Program Change"])
        for loop in range(len(self.ACTION_TEXTS)):
            self.combobox.addItem(self.ACTION_TEXTS[loop])
    def clear_direct_connects(self):
        for loop in range(len(self.ACTION_TEXTS), len(self.combobox)):
            self.combobox.removeItem(loop)
    def add_direct_connect_to_trusted_devices(self, main):
        # Store Current Direct Connect
        item_pos = 0x00 # Default Action is 'connect'
        original_pos = self.combobox.currentIndex()
        current_string = self.combobox.currentText()
        #print "looking for trusted device:",  current_string
        self.clear_direct_connects()
        for trusted_device_number in range(len(main.bt_sys.trustedDevices)):   # Add each trusted device
            select_str = main.bt_sys.trustedDevices[trusted_device_number][0]
            select_str += ' - '
            select_str += main.bt_sys.trustedDevices[trusted_device_number][2]
            self.combobox.addItem(select_str)
            if select_str == current_string:
                item_pos = len(self.ACTION_TEXTS) + trusted_device_number
                #print "match!"
        # Re-Set the currentIndex of the combobox:
        if item_pos:
            self.combobox.setCurrentIndex(item_pos)
        elif original_pos < len(self.ACTION_TEXTS):
            self.combobox.setCurrentIndex(original_pos)
        else:
            self.combobox.setCurrentIndex(0)

class GUI_ExtMc_Mapping():
    def __init__(self):
        #self.layout.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter) # Mac OS X Suggested Patch
        self.MESSAGE_TEXT =("note off","note on","key prs","cc","prog ch","ch prs","ptch bnd")
        self.label = QtGui.QLabel("message")
        self.label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.label.setFrameStyle(6) # Thin Box around it
        #self.combobox.lineEdit().setAlignment(QtCore.Qt.AlignCenter)
        self.combobox = QtGui.QComboBox()#["Note On", "Program Change"])
        for loop in range(len(self.MESSAGE_TEXT)):
            self.combobox.addItem(self.MESSAGE_TEXT[loop])
        self.combobox.addItem("<none>")
        self.combobox.setCurrentIndex(len(self.MESSAGE_TEXT)) # Set to '<none>' at startup
    def SetMap(map_number):
        pass
    def lastItemIndex(self):
        return len(self.combobox)-1
        #self.VALID_OPTIONS = 3

class GUI_ExtMc_MidiChannel():
    def __init__(self):
        # Declare all GUI Elements (Widgets)
        #overall = [[QtGui.QLabel, "Map"],[QtGui.QComboBox, ["Note On","Program Change"]]]
        self.label = QtGui.QLabel("channel")
        self.label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.label.setFrameStyle(6) # Thin Box around it
        self.combobox = QtGui.QComboBox()   # ["Note On", "Program Change"])
        for midi_channel in range(16):
             self.combobox.addItem(str(midi_channel+1)) # MIDI Channel is 1-16 for users (0x00-0x0F for developers)

class GUI_ExtMc_MapNum():
    def __init__(self, label_text="number"):
        # Declare all GUI Elements (Widgets)
        #overall = [[QtGui.QLabel, "Map"],[QtGui.QComboBox, range(128)]]
        self.label = QtGui.QLabel(label_text)
        self.label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.label.setFrameStyle(6) # Thin Box around it
        self.combobox = QtGui.QComboBox()
        for loop in range(128):
            self.combobox.addItem(str(loop))

class GUI_ExtMc_MapValue():
    def __init__(self, label_text="value"):
        # Declare all GUI Elements (Widgets)
        #overall = [[QtGui.QLabel, "Map"],[QtGui.QComboBox, range(128)]]
        self.label = QtGui.QLabel(label_text)
        self.label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.label.setFrameStyle(6) # Thin Box around it
        self.combobox = QtGui.QComboBox()
        for loop in range(128):
            self.combobox.addItem(str(loop))


#class GUI_ExternalController_MidiMessageMapper(QtGui.QFrame):
class GUI_ExternalController_MidiMessageMapper():
    def __init__(self, parent=None):
        #super(GUI_ExternalController_MidiMessageMapper, self).__init__(parent)
        # Setup the Widgets
        self.expected_midi_message = []
        self.map = GUI_ExtMc_Mapping()
        self.channel = GUI_ExtMc_MidiChannel()
        self.number = GUI_ExtMc_MapNum()
        self.value = GUI_ExtMc_MapValue()
        self.action = GUI_ExtMc_ActionSel()
        self.learn_enabled = QtGui.QPushButton("learn")
        self.learn_enabled.setCheckable(True)
        self.learn_enabled.setSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Expanding)
        #self.learn_enabled.setMaximumWidth(100)

        #self.test_button = QtGui.QPushButton("test")
        #self.test_button.setCheckable(True)
        #self.test_button.set
        #self.test_button2 = QtGui.QPushButton("test2")
        
        # Setup the Layout
        self.layout = QtGui.QGridLayout()

        self.layout.addWidget(self.learn_enabled,0,0,2,1) # col0

        self.layout.addWidget(self.map.label,0,1) # col1
        self.layout.addWidget(self.map.combobox,1,1)

        self.layout.addWidget(self.channel.label,0,2) # etc ...
        self.layout.addWidget(self.channel.combobox,1,2)

        self.layout.addWidget(self.number.label,0,3)
        self.layout.addWidget(self.number.combobox,1,3)

        self.layout.addWidget(self.value.label,0,4)
        self.layout.addWidget(self.value.combobox,1,4)

        self.layout.addWidget(self.action.label,0,5)
        self.layout.addWidget(self.action.combobox,1,5)



    def generate_midi_message_from_settings(self):
        message = []
        message_buf = self.map.combobox.currentIndex()
        if message_buf < len(self.map.MESSAGE_TEXT):
            message_buf = 0x80 + (message_buf << 4)
            message_buf |= self.channel.combobox.currentIndex() & 0x0F
            message.append(message_buf)
            message.append(self.number.combobox.currentIndex())
            message.append(self.value.combobox.currentIndex())
            self.expected_midi_message = message[0:3]
            return message
        else: # Case No Message
            return []

    def validate_midilearn_message(self, message):
        if len(message) < 3:
            return False
        elif not message[0] & 0x80:
            return False
        elif (message[0] & 0xF0) == 0xF0: # No Sysex Messages
            return False
        elif message[1] & 0x80:
            return False
        elif message[2] & 0x80:
            return False
        else:
            return True

    def learn_midi_message(self, message):
        if not self.validate_midilearn_message(message):
            return False
        else:
            self.expected_midi_message = message[0:3]
            self.populate_settings_from_midi_message(message)
            return True

    def populate_settings_from_expected_message(self):
        msg = self.expected_midi_message[0]-0x80   >> 4
        chan = self.expected_midi_message[0] & 0x0F
        num = self.expected_midi_message[1]
        val = self.expected_midi_message[2]
        self.map.combobox.setCurrentIndex(msg)
        self.channel.combobox.setCurrentIndex(chan)
        self.number.combobox.setCurrentIndex(num)
        self.value.combobox.setCurrentIndex(val)


    def populate_settings_from_midi_message(self, message):
        msg = message[0]-0x80   >> 4
        chan = message[0] & 0x0F
        num = message[1]
        val = message[2]
        self.map.combobox.setCurrentIndex(msg)
        self.channel.combobox.setCurrentIndex(chan)
        self.number.combobox.setCurrentIndex(num)
        self.value.combobox.setCurrentIndex(val)
