# midi_monitor.py is a simple 
# Special Libraries: Readline/ Python Port MIDI/ Python-Qt4

# Compiling The Program
# OS X: python setup_*.py bdist_mac
# Windows/Linux: python setup_*.py build
# Output Directory: build/

application_title = "Moodi MIDI Monitor" #what you want to application to be called
main_python_file = "midi_monitor.py" #the name of the python file you use to run the program

from gui.sw_version import MOODI_MIDI_MONITOR_VERSION
import sys
import distutils
import os
from cx_Freeze import setup, Executable

includes = ["atexit","re","sip","readline"]
includeimages = [('gui/image_lib/moodi_midi_monitor_logo_square.png')]

# Properties for object that we wish to build
target_name = "moodi_midi_monitor"
if sys.platform == "win32":  # Windows
    packages = []
    #packages = ['scipy.sparse.linalg', 'scipy.integrate']
    base = "Win32GUI"  # This option forces console window NOT to open
    includefiles = [("C:\WINDOWS\system32\pm_dll.dll","pm_dll.dll")]#, ('c:\\Python27\\lib\\site-packages\\scipy\\special\\_ufuncs_cxx.pyd', '_ufuncs_cxx.pyd')]
    includefiles += includeimages
    #includefiles += [('c:\\Python27\\lib\\site-packages\\numpy\\core\\multiarray.pyd', 'multiarray.pyd'), ('c:\\Python27\\lib\\site-packages\\numpy\\core\\multiarray_tests.pyd', 'multiarray_tests.pyd'), ('c:\\Python27\\lib\\site-packages\\numpy\\core\\operand_flag_tests.pyd', 'operand_flag_tests.pyd'), ('c:\\Python27\\lib\\site-packages\\numpy\\core\\scalarmath.pyd', 'scalarmath.pyd'), ('c:\\Python27\\lib\\site-packages\\numpy\\core\\struct_ufunc_test.pyd', 'struct_ufunc_test.pyd'), ('c:\\Python27\\lib\\site-packages\\numpy\\core\\test_rational.pyd', 'test_rational.pyd'), ('c:\\Python27\\lib\\site-packages\\numpy\\core\\umath.pyd', 'umath.pyd'), ('c:\\Python27\\lib\\site-packages\\numpy\\core\\umath_tests.pyd', 'umath_tests.pyd'), ('c:\\Python27\\lib\\site-packages\\numpy\\core\\_dummy.pyd', '_dummy.pyd'), ('c:\\Python27\\lib\\site-packages\\numpy\\fft\\fftpack_lite.pyd', 'fftpack_lite.pyd'), ('c:\\Python27\\lib\\site-packages\\numpy\\lib\\_compiled_base.pyd', '_compiled_base.pyd'), ('c:\\Python27\\lib\\site-packages\\numpy\\linalg\\lapack_lite.pyd', 'lapack_lite.pyd'), ('c:\\Python27\\lib\\site-packages\\numpy\\linalg\\_umath_linalg.pyd', '_umath_linalg.pyd'), ('c:\\Python27\\lib\\site-packages\\numpy\\numarray\\_capi.pyd', '_capi.pyd'), ('c:\\Python27\\lib\\site-packages\\numpy\\random\\mtrand.pyd', 'mtrand.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\cluster\\_hierarchy_wrap.pyd', '_hierarchy_wrap.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\cluster\\_vq.pyd', '_vq.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\fftpack\\convolve.pyd', 'convolve.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\fftpack\\_fftpack.pyd', '_fftpack.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\integrate\\lsoda.pyd', 'lsoda.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\integrate\\vode.pyd', 'vode.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\integrate\\_dop.pyd', '_dop.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\integrate\\_odepack.pyd', '_odepack.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\integrate\\_quadpack.pyd', '_quadpack.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\interpolate\\dfitpack.pyd', 'dfitpack.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\interpolate\\interpnd.pyd', 'interpnd.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\interpolate\\_fitpack.pyd', '_fitpack.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\interpolate\\_interpolate.pyd', '_interpolate.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\linalg\\calc_lwork.pyd', 'calc_lwork.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\linalg\\_cblas.pyd', '_cblas.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\linalg\\_clapack.pyd', '_clapack.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\linalg\\_fblas.pyd', '_fblas.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\linalg\\_flapack.pyd', '_flapack.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\linalg\\_flinalg.pyd', '_flinalg.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\linalg\\_interpolative.pyd', '_interpolative.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\ndimage\\_nd_image.pyd', '_nd_image.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\ndimage\\_ni_label.pyd', '_ni_label.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\odr\\__odrpack.pyd', '__odrpack.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\optimize\\minpack2.pyd', 'minpack2.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\optimize\\moduleTNC.pyd', 'moduleTNC.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\optimize\\_cobyla.pyd', '_cobyla.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\optimize\\_lbfgsb.pyd', '_lbfgsb.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\optimize\\_minpack.pyd', '_minpack.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\optimize\\_nnls.pyd', '_nnls.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\optimize\\_slsqp.pyd', '_slsqp.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\optimize\\_zeros.pyd', '_zeros.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\signal\\sigtools.pyd', 'sigtools.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\signal\\spline.pyd', 'spline.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\signal\\_spectral.pyd', '_spectral.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\spatial\\ckdtree.pyd', 'ckdtree.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\spatial\\qhull.pyd', 'qhull.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\spatial\\_distance_wrap.pyd', '_distance_wrap.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\special\\specfun.pyd', 'specfun.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\special\\_ufuncs.pyd', '_ufuncs.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\special\\_ufuncs_cxx.pyd', '_ufuncs_cxx.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\stats\\futil.pyd', 'futil.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\stats\\mvn.pyd', 'mvn.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\stats\\statlib.pyd', 'statlib.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\stats\\vonmises_cython.pyd', 'vonmises_cython.pyd'), ('c:\\Python27\\lib\\site-packages\\scipy\\stats\\_rank.pyd', '_rank.pyd')]
    target_name += ".exe"
    build_base_dir = "build/moodi_midi_monitor"
    build_dir_extension = "exe.%s-%s" % (distutils.util.get_platform(), sys.version[0:3])
    build_final_dir = os.path.join(build_base_dir, build_dir_extension)                                    
elif sys.platform == "darwin": # Mac
    #packages = ['scipy.sparse.linalg', 'scipy.integrate']
    packages = []
    base = None
    #base = "Console"
    includefiles = [("/usr/local/Cellar/portmidi/217/lib/libportmidi.dylib", "libportmidi.dylib")]
    includefiles += includeimages
    #build_base_dir = "build" # Note: CX-Freeze on OS X will not work successfully when we customize build location.
    #build_dir_extension = ""
    #build_final_dir = build_base_dir
else: # Linux
    packages = []
    base = "Console"
    includefiles = [("/usr/lib/libportmidi.so.0", "libportmidi.so.0"), 
	("/usr/lib/libporttime.so.0","libporttime.so.0")]
    includefiles += includeimages
    build_base_dir = "build/moodi_midi_monitor"
    build_dir_extension = "exe.%s-%s" % (distutils.util.get_platform(), sys.version[0:3])
    build_final_dir = os.path.join(build_base_dir, build_dir_extension)                                    

print base

# Object to Build
Exe_Target = Executable(
    # what to build
    script = "midi_monitor.py",
    initScript = None,
    base = base,
    targetName = target_name,
    compress = True,
    copyDependentFiles = True,
    appendScriptToExe = False,
    appendScriptToLibrary = False,
    icon = None
)

# Build Destination
if sys.platform != "darwin": # Mac:
    setup(
        name = application_title,
        version = MOODI_MIDI_MONITOR_VERSION,
        description = "Moodi MIDI Monitor",
        options = {"build_exe" : {"build_exe" : build_final_dir,"includes" : includes,'include_files':includefiles,'packages': packages }},
        executables = [Exe_Target]
        )
else: #if sys.platform == "darwin": # Mac
    setup(
        name = application_title,
        version = MOODI_MIDI_MONITOR_VERSION,
        description = "Moodi MIDI Monitor",
        options = {"build_exe" : {"includes" : includes, 'include_files':includefiles }},
        executables = [Exe_Target]
        )
