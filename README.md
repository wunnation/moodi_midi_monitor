# moodi_midi_monitor

Simple Cross-Platform MIDI Monitor and MIDI Message Sending Application. 
GUI was developed in PyQt4
Initial version supports Python-PortMIDI backend.
Subsequent versions are expected to also support RtMIDI.
